import mongoose from 'mongoose'

const userSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true,
            unique: true
        },
        password: {
            type: String,
            required: true
        },
        role: {
            type: String,
            default: 'user'
        },
        root: {
            type: Boolean,
            default: false
        },
        avatar: {
            type: String,
            default: 'https://previews.123rf.com/images/panyamail/panyamail1809/panyamail180900343/109879063-user-avatar-icon-sign-profile-symbol.jpg'
        }
    },{
        timestamps: true
}) 

let Dataset = mongoose.models.user || mongoose.model('user', userSchema)
export default Dataset