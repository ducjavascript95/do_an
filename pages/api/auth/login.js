import connectDB from '../../../utils/connectDB'
import Users from '../../../models/userModel'
import jwt from 'jsonwebtoken'
import { createAccessToken} from '../../../utils/generateToken'

connectDB()

export default async (req, res) =>{
   try{
       const rf_token = req.cookies.refreshtoken;
       if(!rf_token) return res.status(400).json({err: 'Please login now!'})

       const result = jwt.verify(rf_token, process.env.REFRESH_TOKEN_SECRET)
       if(!rf_token) return res.status(400).json({err: 'Please login now!'})
   }catch{

   }
}
